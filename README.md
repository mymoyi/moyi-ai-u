
## 本项目除CDN外，全采用自有部署，未调用任何外网API，


# AI绘画壁纸项目
我感觉AI还是很方便的，很适合用来做创意设计
即立项做了这个项目 项目前端将按制作进度随时更新
感谢 “图鸟UI” 的开源
感谢 “Stable Diffusion” 及其 “webui” 的开源
感谢 “ChatGLM-6B” 的开源对话模型
感谢 “Helsinki-NLP” 的开源翻译模型

# 项目预览
![qrcode](data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAGQCAIAAAAP3aGbAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAJ/ElEQVR4nO3dQY4cuxFAwRlD97+yvPnwyk0LNJHKJ0VsBVVVV/c8cJEgv3/+/PkFUPCv3/0AAL9KsIAMwQIyBAvIECwgQ7CADMECMgQLyBAsIEOwgAzBAjIEC8gQLCBDsIAMwQIyBAvI+HHxf76/v58/x5hPGxYePtTkHodj73bJxo13n/fw8G9f4N1bev6hnt9rg7vPa4UFZAgWkCFYQIZgARmCBWQIFpAhWECGYAEZN4OjB91xxLFZxGtjg4V3M7STI6AXT/j8S5x8Sxe6f4lnVlhAhmABGYIFZAgWkCFYQIZgARmCBWQIFpDxeHD04PkI2YbRuOfPMPaW9n8dY4Oydzea/F9v7f/qD6ywgAzBAjIEC8gQLCBDsIAMwQIyBAvIECwgY25wdLkl24oebNjicslbejupODlIefcl8h9WWECGYAEZggVkCBaQIVhAhmABGYIFZAgWkGFw9B9j+1ue77V8enDyLd1dsLvDKr/CCgvIECwgQ7CADMECMgQLyBAsIEOwgAzBAjLmBke7g3bPp0Of3+vC88ebnIY9PManfxrblHW/7l/ilxUWECJYQIZgARmCBWQIFpAhWECGYAEZggVkPB4c7c7gTR4E//xeb59wyePdzTde7Dia/n4vrpZmhQVkCBaQIVhAhmABGYIFZAgWkCFYQIZgARnf6e0Hf7vJzUjHLPlQY4OUSzYjXf6rWMIKC8gQLCBDsIAMwQIyBAvIECwgQ7CAjLk5rCWDMBcbvL290bW3Tzh58vPyaaYN++2dL3jw9mc2+aHuLmiFBWQIFpAhWECGYAEZggVkCBaQIVhAhmABGSs28Jsc2ry41/ORyA0X3H+G89vH2D/MOTZeu2FO+5oVFpAhWECGYAEZggVkCBaQIVhAhmABGYIFZNwMjj4fEby711sbhjmvL3jwdnD0YMmOlG8Hg+90/0D2jxNbYQEZggVkCBaQIVhAhmABGYIFZAgWkCFYQMaPi/+zZETwYGxPxSXns1/ca8PE5vW93t5o8ps6uPtGlr9AO44Cfy/BAjIEC8gQLCBDsIAMwQIyBAvIECwg42ZwdP/M4dgk2/7NSN9ORT6fsXz+ed9+v5M7cN652FH24mrXF3zOCgvIECwgQ7CADMECMgQLyBAsIEOwgAzBAjJujqq/vNOOo64vbnSw/8D3t5ZMD749j37JCOjBhte+5O/XCgvIECwgQ7CADMECMgQLyBAsIEOwgAzBAjJW7Dj63NsnvPtQz1/F2w81+SU+n7F8O885+f3acfT/ZIUFZAgWkCFYQIZgARmCBWQIFpAhWECGYAEZN4OjB3dzcc/H1d4OFu6fsRzb/XLJYOHFY+zfUfZv+xLvWGEBGYIFZAgWkCFYQIZgARmCBWQIFpAhWEDGzeDohuG3s+UzeJNnoy8fBdzwW3r+ijZ8qCW/zOfv1goLyBAsIEOwgAzBAjIEC8gQLCBDsICMxxv43Vk+CLNhrOb8GG8t2U9xw9HKS6aZLiyZv3v+/VphARmCBWQIFpAhWECGYAEZggVkCBaQIVhAxs3g6OSwX3d68GDDecKTL/Zwr+4gZff85A1zrdessIAMwQIyBAvIECwgQ7CADMECMgQLyBAsIOP77ZDb5OnEY/NvS05jXj6p+HywsPvLvLP897zhN/ZlhQWECBaQIVhAhmABGYIFZAgWkCFYQIZgARlzO47eTe6NnRU+ttXn2fLpwecXHNsIdMlemsu/3/0jzVZYQIZgARmCBWQIFpAhWECGYAEZggVkCBaQcTM4umRE8GDJ7oifjE3Dnv/XxdXuPB9HHLva8xnaJfOrFyYHoQ+ssIAMwQIyBAvIECwgQ7CADMECMgQLyBAsIGPFUfUbLJnom9wIdOxGk1vUXtzoYMkg9JLH+GTsl/llhQWECBaQIVhAhmABGYIFZAgWkCFYQIZgARnbB0c3zMXd6b6KyQ1RxyyZhj14+9on53gnv18rLCBDsIAMwQIyBAvIECwgQ7CADMECMgQLyLgZHF1y0vrY+eyTM4d3Fzz42/aA3TAIvWT32k8mf0jPZ0qtsIAMwQIyBAvIECwgQ7CADMECMgQLyBAsIOPH736A/+Fu8Oxicm/JhooHy4+qv3uGscnM51/i81/mwfJx08PjPf+zssICMgQLyBAsIEOwgAzBAjIEC8gQLCDj8RzW2H571xf89L/unmHDgMzZ2BMuOSR5g8mZvg0bcE6ywgIyBAvIECwgQ7CADMECMgQLyBAsIEOwgIybk59Pl9txUu6G+bfnI5Fj+wjuf+fdackNv8yDJXtVHlhhARmCBWQIFpAhWECGYAEZggVkCBaQIVhAxuPB0dOdFszFfb3ecfTiRud7jY2bbphQvX6MsR1lJ03+YN7eaPK3ZIUFZAgWkCFYQIZgARmCBWQIFpAhWECGYAEZj4+qP3g+QvZ2yO35VpDLz2d/Pgc4uU/phn07N3yJdzfq7oX7ZYUFhAgWkCFYQIZgARmCBWQIFpAhWECGYAEZNzuOTm5LODY9ODnXemfD3pLPLR+vPViyA+fFMyxx9yqssIAMwQIyBAvIECwgQ7CADMECMgQLyBAsIOPxUfXPt528u9fYNOydDQeC2yD0V3hLv8JR9QD/hWABGYIFZAgWkCFYQIZgARmCBWQIFpAxt+PonQ0jcxvmPM/G7jU5GPz8MTbYMB16sGS89sAKC8gQLCBDsIAMwQIyBAvIECwgQ7CADMECMh7vONrV3Sv1+l4Xz/B8uPGP/LyTF7ywZNdTg6PAH06wgAzBAjIEC8gQLCBDsIAMwQIyBAvI+HHxfzZsjXjt07ja85G5yenQMZM7Ut5dcOzdLtk79NO9JkeaJz+vFRaQIVhAhmABGYIFZAgWkCFYQIZgARk3c1gHSyaMLgZD0sNlF+Mzz4fLDjacj/38Rn/k3n53V5v8q7fCAjIEC8gQLCBDsIAMwQIyBAvIECwgQ7CAjMeDowdLTo4du9GSGdpPnn+oDXOPd4+xZKZ08oJdVlhAhmABGYIFZAgWkCFYQIZgARmCBWQIFpAxNzi63JKTcsfmOSdHIiff0sUTLhnxHfvBHK42efLz3Wu3wgIyBAvIECwgQ7CADMECMgQLyBAsIEOwgAyDo/+4G5mb3Ix0bHrwYMOh82dj95r8vGPf790Fn8+UHlhhARmCBWQIFpAhWECGYAEZggVkCBaQIVhAxtzg6JJ9HT+ZPDN9+fjl5BzghnHEJZ/3YGyH1f0/WissIEOwgAzBAjIEC8gQLCBDsIAMwQIyBAvI+N6wmeGk5Z93w7TknQ0jr+fH2GDDefRLOKoe+MMJFpAhWECGYAEZggVkCBaQIVhAhmABGTeDowC/hRUWkCFYQIZgARmCBWQIFpAhWECGYAEZggVkCBaQIVhAhmABGYIFZAgWkCFYQIZgARmCBWQIFpDxb9dczk1HuwSwAAAAAElFTkSuQmCC)

# 项目思路以及流程
 - stable-diffusion-webui 插件 
* 注册本地各个调用接口，以及采集模型数据
* 增加nsfw内容审核以及打标签的服务
> 暂时还不怎么会写插件，目前是在别人的插件之上拼凑的代码，一塌糊涂暂不公开

 - py脚本端口  
* 还没弄明白webui插件是怎么运行的，所以只有另外开一个脚本
* 向服务器申请加入注册绘图服务，目前是轮训方式（计划是改成长连接）
* 调用本地绘图插件的接口开始工作，干就完了，奥力给！～
* [https://gitee.com/mymoyi/moyi-ai-m](https://gitee.com/mymoyi/moyi-ai-m)

 - 基于hypef的API  
* 提供注册绘图服务的各种接口
* [https://gitee.com/mymoyi/moyi-ai-h](https://gitee.com/mymoyi/moyi-ai-h)

 - 基于fastadmin的后台管理服务
* 基本就是CURD出来的后台操作没有更多功能
* [https://gitee.com/mymoyi/moyi-ai-a](https://gitee.com/mymoyi/moyi-ai-a) 
* [演示后台管理端](https://ai.moyi.vip/CgpHojxcIM.php)

 - 基于uni-app的前端
* [https://gitee.com/mymoyi/moyi-ai-u](https://gitee.com/mymoyi/moyi-ai-u)
* [dcloud插件市场](https://ext.dcloud.net.cn/plugin?id=11542)

# 0.3.34
 - 注释掉了画幅控制
 - 新增图片长宽控制

# 0.3.29
 - 增加引导提示词系数
 - 增大调整画幅大小

# 0.3.27
 - 调整模型详情页面
  - 新增Lora/采样器/编码器的详情
 - 调整布局绘图页面
  - 修复横竖画幅被我弄反了
  - 新增采样步数
  - 新增lora模型展示
  - 默认画幅竖向
 - 调整制作同款的任务值

# 0.3.25
 - 新增编码器vae、采样器选项
 - 新增中文提示词

# 0.3.23
 - 修复英文撑开布局
 
# 0.3.16
 - 改正错别字

# 0.3.5
 - 新增绘图页面
 - 增加制作同款入口

# 0.2.14
 - 修复注册服务器无归属用户导致模型页面错误的BUG
 - 就一个页面，暂移除底部菜单
 
# 0.2.0
 - 增加模型标签页面

# 0.1.53
 - 从服务器请求内容j
 - 调整列表参数

# 0.1.12
 - 前端静态页面